// named exxport.
export const apiKey = 'abc123';
export const url = 'http://wesbos.com';
// Default export.
// export default apiKey;

// Exporting function.
export function sayHi(name) {
  console.log(`Hello there ${name}`);
}

const age = 100;
const dog = 'snikers';

export { age, dog };
