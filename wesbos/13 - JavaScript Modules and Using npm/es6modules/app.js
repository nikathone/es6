import { uniq } from 'lodash';
import insane from 'insane';
import jsonp from 'jsonp';
import { apiKey, url, sayHi } from './src/config';

import User, { createUrl, gravatar } from './src/user';

// console.log(apiKey, url);

// sayHi('Nikathone');

// const ages = [1, 1, 4, 52, 12, 4];
// console.log(uniq(ages));

const wes = new User('Wes Bos', 'wesbos@gmail.com', 'wesbos.com');
const profile = createUrl(wes.name);
const image = gravatar(wes.email);
console.log(image);
